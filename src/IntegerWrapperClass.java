
public class IntegerWrapperClass {
    public static void main(String[] args){
        IntegerWrapper number=new IntegerWrapper(25) ;
        System.out.println(number.getNumberInObject());

        IntegerWrapper number2=new IntegerWrapper(30);
        int a=number2.getUnboxing(number);
        System.out.println(a);
        System.out.println(number.getNumberInObject()==a );
    }
}
class IntegerWrapper{
    public int numberToConvert;
    public IntegerWrapper(int number) {
        this.numberToConvert =number;
    }

    public int getNumberInObject() {
        return this.numberToConvert;
    }

    public int getUnboxing(IntegerWrapper other){
        int primitiveInteger=other.getNumberInObject() ;
        return primitiveInteger ;
    }
}
